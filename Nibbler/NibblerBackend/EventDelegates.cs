﻿namespace NibblerBackend
{
    //Delegate used by Caterpillar as class member
    //When caterpillar loses a life because it collided in itself or it grew in itself or in a ICollidable object
    //GameState(will reset values for next round) and ScoreAndLives(will decrease nb of Lives) are notified
    public delegate void DeadlyCollisionEventHandler();

    //Delegate for iCollidable
    public delegate void CollisionEventHandler(ICollidable element);

    //Delegate for ScoreAndLives
    public delegate void NoMoreLivesEventHandler();

    //Delegate for Grid whenever a new token is added
    public delegate void TokenAddedEventHandler(Point coords);
}
