﻿using NibblerBackend.Collidables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NibblerBackend
{
    
    public class Caterpillar
    {
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Fields
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //Reference to Grid so Caterpillar knows if it is colliding in something. Top left => (0,0)
        public Grid Grid { get; private set; } 

        //Points occupied by Caterpillar are kept in a queue, when Caterpillar moves of one point = Enqueue 1 point + Dequeue 1 point
        //Front of caterpillar = back of queue
        //Points returns a IEnumerable where idx=Count-1 is the head of the caterpillar and idx=0 is the end of the caterpillar
        private Point frontElement;
        public IEnumerable<Point> Points { get { return points; } }
        private Queue<Point> points;

        //Direction in which Caterpillar is moving into
        public Direction Direction { get; private set; } //Current
        private Direction nextDirection; //Future direction - will be set at next Update() call

        //Initial Caterpillar state
        public const Direction DefaultDirection = Direction.UP; //Initial
        public const int DefaultLength = 5;
        public readonly int DefaultFrontElementX;
        public readonly int DefaultFrontElementY;

        //When caterpillar loses a life because it collided in itself or in an ICollidable,
        //observers (GameState and ScoreAndLives) are notified.
        public event DeadlyCollisionEventHandler DeadlyCollision;

        //When a collision occured which makes the Caterpillar grow, keep track of how many
        //times Update needs to make the Caterpillar advance, without dequeueing 1 point (to make it grow)
        private int nbOfPointsToGrow;

        //Is Caterpillar alive or not
        private bool alive;

        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Constructor
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public Caterpillar(Grid grid)
        {
            Grid = grid;
            points = new Queue<Point>();

            DefaultFrontElementX = Grid.Length / 2 - 1;
            DefaultFrontElementY = Grid.Height / 2 + 1;
            _setCaterpillarAtDefaultDirectionPositionAndLength();

            nbOfPointsToGrow = 0;
            alive = true;
        }

        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Public methods
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Will be taken in account next time Update() is called
        /// </summary>
        public void ChangeDirection(Direction d)
        {
            nextDirection = d;
        }

        /// <summary>
        /// Method called when caterpillar moves.
        /// Each time the Caterpillar moves, most of the tiles remain the same, 
        /// but the rear of the Caterpillar is removed and replaced at the front.
        /// We remove the Point that represents the back of the caterpillar and replace it 
        /// with a new Point that represents the front of the caterpillar.
        /// </summary>
        public void Update()
        {
            //Update direction in case user changed direction
            Direction previousDirection = this.Direction;
            Direction = nextDirection;
            
            switch (Direction)
            {
                case Direction.UP:
                    if (previousDirection == Direction.DOWN) _moveInReverseDirection();
                    else _move(_getNextFrontElementCoords());
                    break;
                case Direction.DOWN:
                    if (previousDirection == Direction.UP) _moveInReverseDirection();
                    else _move(_getNextFrontElementCoords());
                    break;
                case Direction.RIGHT:
                    if (previousDirection == Direction.LEFT) _moveInReverseDirection();
                    else _move(_getNextFrontElementCoords());
                    break;
                case Direction.LEFT:
                    if (previousDirection == Direction.RIGHT) _moveInReverseDirection();
                    else _move(_getNextFrontElementCoords());
                    break;
            }
        }

        /// <summary>
        /// Caterpillar dies, so there is no more points occupied by it on grid.
        /// </summary>
        public void Die()
        {
            while (points.Count > 0)
            {
                points.Dequeue();
            }
            alive = false;
        }

        /// <summary>
        /// Caterpillar is set back to its default position, length and direction.
        /// </summary>
        public void Reset()
        {
            Die();
            _setCaterpillarAtDefaultDirectionPositionAndLength();
            alive = true;
        }

        /// <summary>
        /// Shrinks this Caterpillar object of n points, until the minimum length is reached of Caterpillar.defaultLength
        /// </summary>
        public void Shrink(int n)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException("n", "Must be a positive number.");
            }

            for (int i = 0; i < n && points.Count > DefaultLength; i++)
            {
                 points.Dequeue();
            }
        }

        /// <summary>
        /// Will make the Caterpillar grow of n points in the Direction it is heading. Might cause a collision with itself or an ICollideable.
        /// Throws ArgumentOutOfRangeException if n < 0;
        /// </summary>
        public void Grow(int n)
        {
            if (n < 0)
            {
                throw new ArgumentOutOfRangeException("n", "Must be a positive number.");
            }

            if (alive) nbOfPointsToGrow = n;
        }

        /// <summary>
        /// True if this point is occupied by Caterpillar, false otherwise.
        /// </summary>
        public bool Contains(Point searched)
        {
            foreach (Point p in points)
            {
                if (p.Equals(searched))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Fires an event for subscribers needing to know when caterpillar loses a life
        /// </summary>
        protected virtual void OnDeadlyCollision()
        {
            DeadlyCollision?.Invoke();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y < Grid.Length; y++)
            {
                for (int x = 0; x < Grid.Height; x++)
                {
                    if (Grid[x, y] is Wall) sb.Append('X');
                    else if (Grid[x, y] is CaterpillarGrower) sb.Append('G');
                    else if (Grid[x, y] is CaterpillarShrinker) sb.Append('S');
                    else if (Contains(new Point(x, y))) sb.Append('C');
                    else sb.Append('-');
                }
                sb.Append('\n');
            }

            sb.Append("Direction : " + this.Direction + '\n');
            sb.Append("Head : " + this.frontElement + '\n');
            sb.Append("Points Count : " + this.Points.Count() + '\n');

            sb.Append("Points: {");
            foreach (Point p in this.Points)
            {
                sb.Append(p + " ");
            }
            sb.Append("}" + '\n');

            return sb.ToString();
        }
        
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Helper methods
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void _setCaterpillarAtDefaultDirectionPositionAndLength()
        {
            //Initial state: Caterpillar is going UP
            //Before any player input, Direction and nextDirection have the same value.
            Direction = DefaultDirection;
            nextDirection = Direction;

            //Initial position of caterpillar is at the middle of the grid, going up
            Point p = new Point(DefaultFrontElementX, DefaultFrontElementY);

            //Let's enqueue the points occupied by Caterpillar to the queue "points" until there is no more room
            frontElement = p;
            points.Enqueue(p);

            bool wasInsertionSuccessfull;
            for (int i = 1; i < DefaultLength; i++) //Begin from 1 as we already done first point
            {
                //Grow caterpillar of one point if there is space
                //wasInsertionSuccessfull "false" if there is no where to insert a point
                wasInsertionSuccessfull = _addNewPoint(_getNextFrontElementCoords());
                if (!wasInsertionSuccessfull) break;
            }
        }

        /// <summary>
        /// Reverse direction of the queue then move of one point in new direction.
        /// Direction have already been updated in Update() before calling this function.
        /// </summary>
        private void _moveInReverseDirection()
        {
            //Reverse queue, update frontElement, call normal move method
            this.points = _reverseQueue(this.points);

            //Reset frontElement to other side
            frontElement = points.Last();

            //Change Direction to the actual direction of the 2 points at the head of the Caterpillar
            Direction = _getDirectionOfCaterpillarHead();
            nextDirection = Direction;

            //Where would be next front element
            Point nextPotentialFrontElement = _getNextFrontElementCoords();

            //Try moving to that next front element
            _move(nextPotentialFrontElement);
        }

        /// <summary>
        /// Make the Caterpillar advance in current direction. 
        /// Standard move: Enqueues a point, dequeues a point.
        /// Move while Caterpillar is set to grow: just enqueues a point. (Back of Caterpillar stays the same)
        /// </summary>
        private void _move(Point nextPotentialFrontElement)
        {
            if (alive)
            {
                _addNewPoint(nextPotentialFrontElement);
            }

            //Rechecks in case, _addNewPoint caused death
            if (alive)
            {
                //Unless the caterpillar is set to grow, remove one point to create the motion
                if (nbOfPointsToGrow <= 0)
                {
                    _removeOnePoint();
                }
                //If caterpillar was set to grow, do not remove a point.
                else
                {
                    nbOfPointsToGrow--;
                }
            }
        }

        /// <summary>
        /// Recursive reversing of queue
        /// </summary>
        private Queue<Point> _reverseQueue(Queue<Point> queue)
        {
            if (queue.Count == 0)
            {
                return queue;
            }
            else
            {
                Point p = queue.Dequeue();
                queue = _reverseQueue(queue);
                queue.Enqueue(p);
                return queue;
            }
        }
        
        /// <summary>
        /// Remove one point from Caterpillar. Possible only if dead or if length higher than the minimum defaultLength
        /// </summary>
        private void _removeOnePoint()
        {
            if (alive)
            {
                if (points.Count > DefaultLength) points.Dequeue();
            }
            else
            {
                if(points.Count > 0) points.Dequeue();
            }
        }
        
        /// <summary>
        /// Method to calculate which next point would be the front element if caterpillar would move 
        /// in curren direction.
        /// </summary>
        private Point _getNextFrontElementCoords()
        {
            Point nextFrontElement = null;
            switch (Direction)
            {
                case Direction.UP:
                    nextFrontElement = new Point(frontElement.X, frontElement.Y - 1);
                    break;
                case Direction.DOWN:
                    nextFrontElement = new Point(frontElement.X, frontElement.Y + 1);
                    break;
                case Direction.RIGHT:
                    nextFrontElement = new Point(frontElement.X + 1, frontElement.Y);
                    break;
                case Direction.LEFT:
                    nextFrontElement = new Point(frontElement.X - 1, frontElement.Y);
                    break;
            }
            return nextFrontElement;
        }

        /// <summary>
        /// Method to calculate which next point would be the front element if caterpillar would move 
        /// in curren direction.
        /// </summary>
        private Point _getReverseNextFrontElementCoords()
        {
            Point nextFrontElement = null;

            //The direction should be based on the one of the two points at the head of the Caterpillar, in
            //case the Caterpillar is in a "L" or "U" form.
            
            switch (Direction)
            {
                case Direction.UP:
                    nextFrontElement = new Point(frontElement.X, frontElement.Y - 1);
                    break;
                case Direction.DOWN:
                    nextFrontElement = new Point(frontElement.X, frontElement.Y + 1);
                    break;
                case Direction.RIGHT:
                    nextFrontElement = new Point(frontElement.X + 1, frontElement.Y);
                    break;
                case Direction.LEFT:
                    nextFrontElement = new Point(frontElement.X - 1, frontElement.Y);
                    break;
            }
            return nextFrontElement;
        }

        private Direction _getDirectionOfCaterpillarHead()
        {
            //For when this method is first called to initialize points.
            if (points.Count < 2)
            {
                return DefaultDirection;
            }

            Point secondElement = points.ElementAt(points.Count() - 2);
            //Points are either next to each other or one is above the other one.
            if (frontElement.X != secondElement.X)
            {
                if (frontElement.X < secondElement.X) return Direction.LEFT;
                else return Direction.RIGHT;
            }
            else if (frontElement.Y != secondElement.Y)
            {
                if (frontElement.Y < secondElement.Y) return Direction.UP;
                else return Direction.DOWN;
            }

            throw new Exception("Error in direction of two first points of Caterpillar.");
        }

        /// <summary>
        /// Adds new point if it doens't create a collision. If it collides with another object,
        /// it calls the object's collide method, if it collides within itself it invokes the event delegate.
        /// Return true if point was added without any collision, false if it was added and there was a collision.
        /// </summary>
        private bool _addNewPoint(Point nextFrontElement)
        {
            ICollidable contentOfNextFrontElement = Grid[nextFrontElement.X, nextFrontElement.Y];
            bool hasACollisionOccured = false;

            //Checks if it collided with itself
            if (Contains(nextFrontElement))
            {
                OnDeadlyCollision();
                hasACollisionOccured = true;
            }
            //If it collided with a Token
            else if (contentOfNextFrontElement is CaterpillarGrower || contentOfNextFrontElement is CaterpillarShrinker)
            {
                points.Enqueue(nextFrontElement);
                frontElement = nextFrontElement;
                contentOfNextFrontElement.Collide(this);
                hasACollisionOccured = true;
            }
            //If it collided with a Wall
            else if (contentOfNextFrontElement is Wall)
            {
                contentOfNextFrontElement.Collide(this);
                hasACollisionOccured = true;
            }
            //If no collision, proceed to move!
            else
            {
                points.Enqueue(nextFrontElement);
                frontElement = nextFrontElement;
                hasACollisionOccured = false;
            }
            return !hasACollisionOccured;
        }
    }
}