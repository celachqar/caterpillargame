﻿using NibblerBackend.Collidables;

namespace NibblerBackend
{
    public class ScoreAndLives
    {
        public int Score { get; private set; }
        public int Lives { get; private set; }
        public event NoMoreLivesEventHandler NoMoreLives;

        public ScoreAndLives()
        {
            Score = 0;
            Lives = 3;
        }

       /// <summary>
       /// Update the score depending of the collision. Update lives if collision with wall. Score has a minimum of 0.
       /// </summary>
        public void UpdatePointsAndLives(ICollidable c)
        {
            Score += c.Points;
            if (Score < 0) Score = 0;
            if (c is Wall) CaterpillarDies();
        }

        /// <summary>
        /// If Caterpillar has no more lives, notify subscribers to NoMoreLives (GameState.SetPause())
        /// </summary>
        public void CaterpillarDies()
        {
            Lives--;
            if (Lives == 0) NoMoreLives?.Invoke();
        }

        public void Reset()
        {
            Score = 0;
            Lives = 3;
        }
    }
}