﻿using System;

namespace NibblerBackend
{
    //Class representing a point in the game Grid
    public class Point
    {
        public int X;
        public int Y;

        public Point(int x, int y)
        {
            if(x < 0 || y < 0)
            {
                throw new ArgumentOutOfRangeException("Coordinates must be positive.");
            }
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            var point = obj as Point;
            return point != null &&
                   X == point.X &&
                   Y == point.Y;
        }
        public override int GetHashCode()
        {
            return (17 + (23 * X.GetHashCode())) * (23 * Y.GetHashCode());
        }

        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }
    }
}
