﻿namespace NibblerBackend
{
    public interface ICollidable
    {
        int Points { get; }

        event CollisionEventHandler Collision;

        void Collide(Caterpillar c);
    }
}
