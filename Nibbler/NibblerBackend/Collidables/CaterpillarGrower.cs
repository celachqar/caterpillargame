﻿namespace NibblerBackend.Collidables
{
    public class CaterpillarGrower : ICollidable
    {
        public int Points { get; private set; }

        public int Growth { get; private set; }

        public event CollisionEventHandler Collision;

        public CaterpillarGrower(int grow)
        {
            Growth = grow;
            Points = 100;
        }

        public void Collide(Caterpillar c)
        {
            Collision?.Invoke(this);
            c.Grow(Growth);
        }
    }
}
