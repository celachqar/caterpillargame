﻿namespace NibblerBackend.Collidables
{
    public class CaterpillarShrinker : ICollidable
    {
        public int Points { get; set; }

        public int Growth { get; private set; }

        public event CollisionEventHandler Collision;

        public CaterpillarShrinker(int shrink)
        {
            Growth = shrink;
            Points = 50;
        }

        public void Collide(Caterpillar c)
        {
            Collision?.Invoke(this);
            c.Shrink(Growth);
        }
    }
}
