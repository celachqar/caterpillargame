﻿using System;

namespace NibblerBackend.Collidables
{
    public class Wall : ICollidable
    {
        public int Points { get; private set; }

        public event CollisionEventHandler Collision;

        public Wall()
        {
            Points = -100;
        }

        public void Collide(Caterpillar c)
        {
            c.Reset();
            Collision?.Invoke(this);
        }
    }
}
