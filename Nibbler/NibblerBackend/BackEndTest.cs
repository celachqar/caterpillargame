using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NibblerBackend;

namespace NibblerBackend
{
    class BackEndTest
    {
        static GameState game;
        public static void Main(string[] args)
        {
            game = GameState.Load("sampleGrid.txt");

            /*
             * Grid grid = new Grid(20, 20, 0.5, null, null);
            Caterpillar c = new Caterpillar(grid);
            //c.Print();
            //Console.ReadKey();
            Console.WriteLine(grid.ToString());
            grid.tiles[0, 0].Collide(c);
            //IMPORTANT
            grid.tiles[10, 10]?.Collide(c);
            //IMPORTANT
            */


            camillia();



            /* LIST DELEGATE TESTING FROM LECTURE 11
            // Create a new list.
            ListWithChangedEvent list = new ListWithChangedEvent();

            list.Changed += new ChangedEventHandler(printThatListChanged); //subscriber adds their method

            // Add and remove items from the list.
            list.Add("item 1");
            list.Add(2);
            list[1] = 5;
            list.Clear();

            Random chance = new Random();
            double choice = (double)chance.Next(100) / 100;
            Console.WriteLine(choice);
            */


           Console.Read();
        }

        

        private static void camillia()
        {
            string sep = "\n" + new string('*', 45) + "\n";

            Console.WriteLine("Initial - Going DOWN"+sep);
            printGameAndPoints();
            /*
            game.cp.ChangeDirection(Direction.UP);
            game.cp.Update();
            Console.WriteLine("Change to UP + 1 step" + sep);
            printGameAndPoints();

            game.cp.Grow(5);
            Console.WriteLine("Grow of 5" + sep);
            printGameAndPoints();
            
            game.cp.Update();
            game.cp.Update();
            Console.WriteLine("2steps" + sep);
            printGameAndPoints();

            game.cp.ChangeDirection(Direction.RIGHT);
            game.cp.Update();
            Console.WriteLine("Change to RIGHT + 1 step" + sep);
            printGameAndPoints();

            game.cp.ChangeDirection(Direction.LEFT);
            game.cp.Update();
            game.cp.Update();
            game.cp.Update();
            Console.WriteLine("Change to LEFT + 3 steps" + sep);
            printGameAndPoints();
            */
            for(int i=0; i <10; i++)
            {
                game.GameCaterpillar.Update();
                Console.WriteLine("1 step" + sep);
                printGameAndPoints();
            }

            game.GameCaterpillar.Reset();
            Console.WriteLine("Reset" + sep);
            printGameAndPoints();

            game.GameCaterpillar.Die();
            Console.WriteLine("Die" + sep);
            printGameAndPoints();

        }

        private static void printGameAndPoints()
        {
            Console.WriteLine(game.GameGrid.ToString());
            Console.WriteLine("Caterpillar - Head :" + game.GameCaterpillar.frontElement);
            Console.WriteLine("Caterpillar is on:");
            foreach (Point p in game.GameGrid.caterpillarPoints.ToList())
            {
                Console.WriteLine(p);

            }
        }
    }
}