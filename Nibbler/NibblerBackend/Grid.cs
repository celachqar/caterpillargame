﻿using NibblerBackend.Collidables;
using System;
using System.Collections.Generic;
using System.Text;

namespace NibblerBackend
{
    /**
     * Grid represents a game grid, which holds ICollidable Wall objects all around and one ICollidable CaterpillarShrinker or ICollidable CaterpillarGrower.
     **/
    public class Grid
    {
        //Array of ICollidables and its size
        public ICollidable[,] Tiles { get; private set; }
        public int Height { get; private set; }
        public int Length { get; private set; }

        //Position of current token
        public Point CurrentTokenPosition { get; protected set; }

        //When a new token is added, observers (GameState._onNewToken()) are notified.
        public event TokenAddedEventHandler TokenAdded;

        //Proportion of times that a “good” token will appear after the existing one is eaten.
        private readonly double shrinkTokenGenChance;

        //badTokenGrowth = {m, n, o} means m% of the time that a bad token is generated, the length goes up by 1, n% of the time, the length goes up by 2, and o% of the time, the length goes up by 3.
        private readonly double[] badTokenGrowth;

        //shrinkTokenGrowth = {m, n} means m% of the time that a bad token is generated, the length goes down by 1, n% of the time, the length goes down by 2.
        private readonly double[] shrinkTokenGrowth;

        //Reference to the list of points occcupied by Caterpillar
        protected IEnumerable<Point> caterpillarPoints;
        
        /// <summary>
        /// Constructor needs to be followed by SetGrid() method.
        /// </summary>
        public Grid(int hei, int len, double stgc, double[] btg, double[] stg)
        {
            if (hei < GameState.MinimumGridSize || len < GameState.MinimumGridSize)
            {
                throw new ArgumentException("Grid dimensions must be of least " + GameState.MinimumGridSize);
            }
            Height = hei;
            Length = len;
            shrinkTokenGenChance = stgc;
            badTokenGrowth = btg;
            shrinkTokenGrowth = stg;
            Tiles = new ICollidable[Length, Height];
            setWalls();
        }
        
        /// <summary>
        /// This method needs to be called after the Caterpillar and the ScoreAndLives in GameState have been constructed.
        /// Once the Grid has this reference, it knows where the Caterpillar is and will set the first token, as well as subscribe it to UpdatePointsAndLives.
        /// </summary>
        public virtual void SetGrid(IEnumerable<Point> caterpillarPoints)
        {
            this.caterpillarPoints = caterpillarPoints;
            setToken();
        }

        /// <summary>
        /// Indexor.
        /// </summary>
        public ICollidable this[int x, int y] { get { return Tiles[x, y]; } }

        /// <summary>
        /// Whenever a token is eaten or a wall is touched, event happens and this method is called to add a new token.
        /// We do not use parameter as we keep in a private field the CurrentTokenPosition informations.
        /// </summary>
        public void AddNewToken(ICollidable element)
        {
            //Remove previous token
            removeToken();
            //Set new one
            setToken();
            //Fires new token event (event handler in GameState _onNewToken())
            TokenAdded(CurrentTokenPosition);
        }

        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Helper methods
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Removes the current token
        private void removeToken()
        {
            Tiles[CurrentTokenPosition.X, CurrentTokenPosition.Y] = null;
        }

        //Sets an ICollidable at a random spot in the array that is not a wall or the caterpillar
        private void setAtRandomAcceptableCoord(ICollidable modifier)
        {
            Random points = new Random();

            //With example length = 10, we want the token to be placed from x = 1 to 8 (not 0 or 9 because walls)
            //To achieve this, random.Next(Length - 2) will give 0 to 7, and + 1 will give 1 to 8.

            int x, y;
            Point pointToCheck;

            do
            {
                x = points.Next(Length - 2) + 1;
                y = points.Next(Height - 2) + 1;
                pointToCheck = new Point(x, y);
            }
            while (caterpillarContains(pointToCheck));

            CurrentTokenPosition = pointToCheck;
            Tiles[x, y] = modifier;
        }

        private bool caterpillarContains(Point searched)
        {
            foreach (Point p in caterpillarPoints)
            {
                if (p.Equals(searched))
                {
                    return true;
                }
            }
            return false;
        }

        //Generates a token, sets it randomly in the grid (not on the caterpilar or walls) and add the event listener (subscribes it)
        private void setToken()
        {
            ICollidable modifier = ShrinkOrGrowToken();
            setAtRandomAcceptableCoord(modifier);
        }

        //Sets the border of the tiles array to ICollidable Walls.
        private void setWalls()
        {
            //top and bottom sides (respectively)
            for (int i = 0; i < Length; i++)
            {
                Tiles[i, 0] = new Wall();
                Tiles[i, Height - 1] = new Wall();
            }

            //left and right sides (respectively)
            for (int i = 0; i < Height; i++)
            {
                Tiles[0, i] = new Wall();
                Tiles[Length - 1, i] = new Wall();
            }
        }

        //Depending on the probability given, decides if next token will be a grower or shrinker
        private ICollidable ShrinkOrGrowToken()
        {
            Random chance = new Random();
            double choice = (double)chance.Next(100) / 100;
            double growth = (double)chance.Next(100) / 100;

            if (choice < shrinkTokenGenChance)
            {
                if (growth < shrinkTokenGrowth[0]) return new CaterpillarShrinker(1);
                else return new CaterpillarShrinker(2);
            }
            else
            {
                if (growth < shrinkTokenGrowth[0]) return new CaterpillarGrower(1);
                else if (growth < shrinkTokenGrowth[0] + shrinkTokenGrowth[1]) return new CaterpillarGrower(2);
                else return new CaterpillarGrower(3);
            }
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            for (int y = 0; y < Length; y++)
            {
                for (int x = 0; x < Height; x++)
                {
                    if (Tiles[x, y] is Wall) sb.Append('X');
                    else if (Tiles[x, y] is CaterpillarGrower) sb.Append('G');
                    else if (Tiles[x, y] is CaterpillarShrinker) sb.Append('S');
                    else if (caterpillarContains(new Point(x, y))) sb.Append('C');
                    else sb.Append('_');
                }
                sb.Append('\n');
            }

            return sb.ToString();
        }
    }
}