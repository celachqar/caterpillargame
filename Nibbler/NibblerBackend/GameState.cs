﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace NibblerBackend
{
    public class GameState
    {
        public Grid GameGrid { get; private set; }
        public Caterpillar GameCaterpillar { get; private set; }
        public ScoreAndLives ScoreLives { get; private set; }
        public bool ShouldContinue { get; private set; }
        public const int MinimumGridSize = 20;
        
        /// <summary>
        /// Instance constructor used by the static Load method.
        /// </summary>
        protected GameState(Grid g, Caterpillar cat, ScoreAndLives s)
        {
            //Grid with ICollideables
            GameGrid = g;

            //Caterpillar
            GameCaterpillar = cat;

            //Gives the grid a reference to the coordinates (points) occupied by Caterpillar so Grid can set
            //itself (including its token) for the game
            GameGrid.SetGrid(cat.Points);

            //Scores and lives
            ScoreLives = s;

            //Set up the events
            _setUpEvents();

            //Set to false when Caterpillar dies
            ShouldContinue = true;
        }

        /// <summary>
        /// Each time Update() is called, the code should update the caterpillar’s position as long as the field ShouldContinue is true. 
        /// </summary>
        public void Update()
        {
            if (ShouldContinue)
            {
                GameCaterpillar.Update();
            }
        }

        /// <summary>
        /// Event Handler called when ScoreAndLives fires the event NoMoreLives or when Caterpiller fires the SelfCollition event.
        /// </summary>
        public void SetPause()
        {
            ShouldContinue = false;
            if (ScoreLives.Lives == 0)
            {
                GameCaterpillar.Die();
            }
            else
            {
                GameCaterpillar.Reset();
                ShouldContinue = true;
            }
        }

        /// <summary>
        /// Load grid fromat and game probabilities from file and create GameState object.
        /// </summary>
        public static GameState Load(string fileName)
        {
            string[] lines;
            var list = new List<string>();
            var fileStream = new FileStream("../../../../" + fileName, FileMode.Open, FileAccess.Read);
            using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            lines = list.ToArray();

            if (validateGrid(lines))
            {
                string stgcData = lines[0];
                string[] btgData = lines[1].Split();
                string[] stgData = lines[2].Split();
                // Ensures that the decimal separator is actually a period and no other symbol
                // Adds support to other languages (i.e. French 1,5 rather than 1.5)
                NumberFormatInfo format = new NumberFormatInfo { NumberDecimalSeparator = "." };

                double stgc = Convert.ToDouble(stgcData, format);
                double[] btg = { Convert.ToDouble(btgData[0], format), Convert.ToDouble(btgData[1], format), Convert.ToDouble(btgData[2], format) };
                double[] stg = { Convert.ToDouble(stgData[0], format), Convert.ToDouble(stgData[1], format) };

                if (validateProbs(stgc, btg, stg))
                {
                    Grid grid = new Grid(lines.Length - 3, lines[3].Length, Convert.ToDouble(lines[0].Split()[0], format), btg, stg);
                    Caterpillar c = new Caterpillar(grid);
                    ScoreAndLives s = new ScoreAndLives();

                    return new GameState(grid, c, s);
                }
                else throw new ArgumentException("Please ensure the probabilities in " + fileName + " are valid!");
            }
            else throw new ArgumentException("The grid in " + fileName + " must be rectangular! (minimum size is (" + MinimumGridSize + ", " + MinimumGridSize + ")");
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Helper methods
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Checks if the grid in the specified array is rectangular. Minimum grid size is 20x20
        private static bool validateGrid(string[] lines)
        {
            //Grid only starts at line 3
            int length = lines[3].Length;
            int width = lines.Length - 3;

            if (length < MinimumGridSize || width < MinimumGridSize) return false;

            //Now that we know the grid is at least the minimum size, check if all the inner lengths are equal (to make a rectangle)
            for (int i = 4; i < lines.Length; i++)
            {
                if (lines[i].Length != length) return false;
            }
            return true;
        }

        //Checks if the probabilities all add up to 1
        private static bool validateProbs(double stgc, double[] btg, double[] stg)
        {
            if (btg[0] + btg[1] + btg[2] == 1 && stg[0] + stg[1] == 1 && stgc < 1) return true;
            else return false;
        }

        /// <summary>
        /// When a new token is added, should set it up with appropriate events.
        /// </summary>
        private void _onTokenAdded(Point coords)
        {
            _setUpEventsForToken(coords);
        }
        
        private void _setUpEvents()
        {
            foreach (ICollidable c in GameGrid.Tiles)
            {
                if (c != null) _setUpEventsForToken(c);
            }
            GameGrid.TokenAdded += _onTokenAdded;
            GameCaterpillar.DeadlyCollision += ScoreLives.CaterpillarDies;
            GameCaterpillar.DeadlyCollision += SetPause;
            ScoreLives.NoMoreLives += SetPause;
        }

        /// <summary>
        /// Beware, this method should set up the same events as other _setUpEventsForToken methods
        /// </summary>
        private void _setUpEventsForToken(ICollidable c)
        {
            c.Collision += GameGrid.AddNewToken;
            c.Collision += ScoreLives.UpdatePointsAndLives;
        }

        /// <summary>
        ///  Beware, this method should set up the same events as other _setUpEventsForToken methods
        /// </summary>
        private void _setUpEventsForToken(Point coords)
        {
            GameGrid.Tiles[coords.X, coords.Y].Collision += GameGrid.AddNewToken;
            GameGrid.Tiles[coords.X, coords.Y].Collision += ScoreLives.UpdatePointsAndLives;
        }
    }
}