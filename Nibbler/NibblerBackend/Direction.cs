﻿namespace NibblerBackend
{
    //Direction in which the Caterpillar is moving within the Grid
    public enum Direction { UP, DOWN, LEFT, RIGHT }
}
