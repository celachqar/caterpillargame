﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NibblerBackend;
using NibblerMonogame.Sprites;

namespace NibblerMonogame
{
    /// <summary>
    /// Main class for game!
    /// </summary>
    public class NibblerGame : Game
    {
        //Game logic
        public GameState gamestate;

        //Sprites
        SpriteBatch spriteBatch;
        private CaterpillarSprite caterpillarSprite;
        private GridSprite gridSprite;
        private ScoreAndLivesSprite scoreAndLivesSprite;

        //Graphics display and coordinates fields
        GraphicsDeviceManager graphics;
        public const double displayMultiplier = 32; // Pixel size
        public const double startGridXcoord = 50;
        public const int windowHeight = (int)(displayMultiplier * 20); // 20 tiles
        public const int windowWidth = (int)(displayMultiplier * 20 + 320);

        //Game is updated every 0.15 seconds
        public const int updateTime = 150;

        //Keeps track of if game has started/was a key pressed by player at some point
        private bool isGameStarted;

        /// <summary>
        /// Constructor
        /// </summary>
        public NibblerGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            isGameStarted = false;
        }

        /// <summary>
        /// Initialize Caterpillar, Grid and scoreAndLives sprites. Arrange window size.
        /// </summary>
        protected override void Initialize()
        {
            gamestate = GameState.Load("sampleGrid.txt");

            gridSprite = new GridSprite(this);
            gridSprite.Initialize();
            Components.Add(gridSprite);

            caterpillarSprite = new CaterpillarSprite(this);
            caterpillarSprite.Initialize();
            Components.Add(caterpillarSprite);

            scoreAndLivesSprite = new ScoreAndLivesSprite(this);
            scoreAndLivesSprite.Initialize();
            Components.Add(scoreAndLivesSprite);

            graphics.PreferredBackBufferHeight = windowHeight;
            graphics.PreferredBackBufferWidth = windowWidth;
            graphics.ApplyChanges();

            base.Initialize();
        }


        /// <summary>
        /// If game is started, calls the game logic GameState Update() method.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape)) Exit();

            //Check if player has pressed a key at some point. If so, start game.
            if (!isGameStarted) _wasKeyPressed();

            //If game has started, update game logic only at every updateTime Milliseconds
            if (isGameStarted && gameTime.TotalGameTime.Milliseconds % updateTime == 0) gamestate.Update();

            if (!gamestate.ShouldContinue) // If the game is over
            {
                resetIfSpacebar();
            }

            base.Update(gameTime);
        }
        
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            base.Draw(gameTime);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// Unused for this project.
        /// </summary>
        protected override void UnloadContent()
        {
            // Unused for this project
        }

        //|///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Helper methods
        //|///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void _wasKeyPressed()
        {
            KeyboardState state = Keyboard.GetState(); // Used to determine which key is being held
            if (state.GetPressedKeys().Count() != 0) isGameStarted = true;
        }

        private void resetIfSpacebar()
        {
            KeyboardState state = Keyboard.GetState(); // Used to determine which key is being held
            if (state.GetPressedKeys().Count() != 0)
            {
                if (state.IsKeyDown(Keys.Space))
                {
                    gamestate.ScoreLives.Reset();
                    gamestate.GameGrid.AddNewToken(null);
                    gamestate.SetPause();
                }
            }
        }
    }
}

