﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace NibblerMonogame.Sprites
{
    public class ScoreAndLivesSprite : DrawableGameComponent
    {
        // Business logic
        private NibblerGame game;
        // To render
        private SpriteBatch spriteBatch;
        private SpriteFont font; // http://rbwhitaker.wikidot.com/monogame-drawing-text-with-spritefonts
        private int score = 0;
        private int lives = 3;

        public ScoreAndLivesSprite(NibblerGame nibbler) : base(nibbler)
        {
            game = nibbler;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = game.Content.Load<SpriteFont>("Stats");

            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            score = game.gamestate.ScoreLives.Score;
            lives = game.gamestate.ScoreLives.Lives;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            if (game.gamestate.ShouldContinue)
            {
                spriteBatch.DrawString(font, "  Score: " + score + "\n  Lives: " + lives, new Vector2((float)(NibblerGame.windowHeight * 1.1), 200), Color.Brown);
            }
            else // If the game is over
            {
                spriteBatch.DrawString(font, " GAME OVER!\n\nYour final score\nis " + score + ".\n\nPress spacebar\nto restart.", new Vector2((float)(NibblerGame.windowHeight * 1.1), 200), Color.Brown);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}