﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NibblerBackend;
using NibblerBackend.Collidables;

namespace NibblerMonogame.Sprites
{
    public class GridSprite : DrawableGameComponent
    {
        // Business logic
        private Grid grid;
        private NibblerGame game;
        // To render
        private SpriteBatch spriteBatch;
        private Texture2D[,] collidableImages;
        
        public GridSprite(NibblerGame nibbler) : base(nibbler)
        {
            game = nibbler;
            grid = game.gamestate.GameGrid;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            collidableImages = new Texture2D[grid.Height,grid.Length];
            base.LoadContent();
        }
        
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            //// Goes through each tile in the grid and assigns a sprite respective to the type and then draw it
            
            ICollidable ic;
            int pX, pY, w, h;
            spriteBatch.Begin();
            
            for (int x = 0; x < collidableImages.GetLength(0); x++)
            {
                for (int y = 0; y < collidableImages.GetLength(1); y++)
                {
                    ic = grid.Tiles[x, y];
                    //Assign appropriate image depending of tile
                    if (ic is CaterpillarGrower) collidableImages[x, y] = game.Content.Load<Texture2D>("corn");
                    else if (ic is CaterpillarShrinker) collidableImages[x, y] = game.Content.Load<Texture2D>("toilet_paper");
                    else if (ic is Wall) collidableImages[x, y] = game.Content.Load<Texture2D>("wall");
                    else collidableImages[x, y] = game.Content.Load<Texture2D>("water");

                    //Translate distances to screen
                    pX = (int)(x* NibblerGame.displayMultiplier + NibblerGame.startGridXcoord);
                    pY = (int)(y * NibblerGame.displayMultiplier);
                    w = (int)NibblerGame.displayMultiplier;
                    h = (int)NibblerGame.displayMultiplier;

                    //Draw
                    spriteBatch.Draw(collidableImages[x,y], new Rectangle(pX,pY,w,h), Color.White);
                }
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
