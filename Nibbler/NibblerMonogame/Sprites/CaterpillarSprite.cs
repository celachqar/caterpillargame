﻿using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NibblerBackend;

namespace NibblerMonogame.Sprites
{
    public class CaterpillarSprite : DrawableGameComponent
    {
        // Business object
        private Caterpillar caterpillar;

        //Game
        private NibblerGame game;

        // To render
        private SpriteBatch spriteBatch;
        private Texture2D caterpillarBody;
        

        public CaterpillarSprite(NibblerGame nibbler) : base(nibbler)
        {
            game = nibbler;
            caterpillar = game.gamestate.GameCaterpillar;
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Load image which will represent Caterpillar
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            caterpillarBody = game.Content.Load<Texture2D>("poop_water");
            base.LoadContent();
        }

        /// <summary>
        /// Update direction of caterpillar if arraow key pressed by player
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime)
        {
            _checkIfKeyPressedAndUpdateCaterpillarDirection();
            base.Update(gameTime);
        }

        /// <summary>
        /// Places the Caterpillar image on every point occupied by Caterpillar on grid
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            int pX, pY, w, h;
            //Put a Caterpillar image for every Point that it occupies
            foreach (NibblerBackend.Point p in caterpillar.Points)
            {
                pX = (int)((p.X * NibblerGame.displayMultiplier) + NibblerGame.startGridXcoord);
                pY = (int)(p.Y * NibblerGame.displayMultiplier);
                w = (int)NibblerGame.displayMultiplier;
                h = (int)NibblerGame.displayMultiplier;
                spriteBatch.Draw(caterpillarBody, new Rectangle(pX, pY, w, h), Color.White);
            }

            spriteBatch.End();
            base.Draw(gameTime);
        }
    
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Helper methods
        //|////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Changes direction of Caterpillar according to user input.
        /// Returns true if a key was pressed, false otherwise
        /// </summary>
        private void _checkIfKeyPressedAndUpdateCaterpillarDirection()
        {
            KeyboardState state = Keyboard.GetState(); // Used to determine which key is being held
            if (state.GetPressedKeys().Count() != 0)
            {
                if (state.IsKeyDown(Keys.Right))
                {
                    caterpillar.ChangeDirection(Direction.RIGHT);
                }
                else if (state.IsKeyDown(Keys.Left))
                {
                    caterpillar.ChangeDirection(Direction.LEFT);
                }
                else if (state.IsKeyDown(Keys.Up))
                {
                    caterpillar.ChangeDirection(Direction.UP);
                }
                else if (state.IsKeyDown(Keys.Down))
                {
                    caterpillar.ChangeDirection(Direction.DOWN);
                }
            }
        }
    }
}