﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NibblerBackend;
using NibblerBackend.Collidables;

namespace NibblerUnitTests
{
    [TestClass]
    public class ScoreAndLivesTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            ScoreAndLives sal = new ScoreAndLives();

            Assert.AreEqual(0, sal.Score); // Should these be in separate TestMethods?
            Assert.AreEqual(3, sal.Lives);
        }

        [TestMethod]
        public void UpdatePointsAndLivesTest1()
        {
            ScoreAndLives sal = new ScoreAndLives();
            sal.UpdatePointsAndLives(new CaterpillarGrower(1)); // Adds 100 to Score of 0
            Assert.AreEqual(100, sal.Score);
        }

        [TestMethod]
        public void UpdatePointsAndLivesTest2()
        {
            ScoreAndLives sal = new ScoreAndLives();
            sal.UpdatePointsAndLives(new CaterpillarGrower(1)); // Adds 100 to Score of 0
            sal.UpdatePointsAndLives(new CaterpillarShrinker(1)); // Adds 50 to Score of 100
            Assert.AreEqual(150, sal.Score);
        }

        [TestMethod]
        public void UpdatePointsAndLivesTest3()
        {
            ScoreAndLives sal = new ScoreAndLives();
            sal.UpdatePointsAndLives(new CaterpillarGrower(1)); // Adds 100 to Score of 0
            sal.UpdatePointsAndLives(new CaterpillarShrinker(1)); // Adds 50 to Score of 100
            sal.UpdatePointsAndLives(new Wall()); // Removes 100 from Score of 150
            Assert.AreEqual(50, sal.Score);
        }

        [TestMethod]
        public void UpdatePointsAndLivesTest4()
        {
            ScoreAndLives sal = new ScoreAndLives();
            sal.UpdatePointsAndLives(new Wall()); // Would immediately crash into a wall, Score would be -100

            Assert.AreEqual(0, sal.Score);
        }

        [TestMethod]
        public void UpdatePointsAndLivesTest5()
        {
            ScoreAndLives sal = new ScoreAndLives();
            sal.UpdatePointsAndLives(new Wall()); // Will lose a life

            Assert.AreEqual(2, sal.Lives);
        }
    }
}
