﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NibblerBackend;
using NibblerBackend.Collidables;

namespace NibblerUnitTests
{
    /// <summary>
    /// Summary description for GridTest
    /// </summary>
    [TestClass]
    public class GridTest
    {
        [TestMethod]
        public void ConstructorTest()
        {
            // Cannot validate these variables as the validation is done in GameState. Assume these are all valid
            Grid grid = new Grid(20, 20, 0.5, new double[3] { 0.25, 0.25, 0.5 }, new double[2] { 0.5, 0.5 });
            Caterpillar c = new Caterpillar(grid);
            grid.SetGrid(c.Points);

            Assert.IsTrue(grid.Tiles[0, 0] is Wall);
        }

        [TestMethod]
        public void SetWallsTest()
        {
            Grid grid = new Grid(20, 20, 0.5, new double[3] { 0.25, 0.25, 0.5 }, new double[2] { 0.5, 0.5 });

            Assert.IsTrue(grid.Tiles[0,0] is Wall);
            
            for (int x = 0; x < grid.Length; x++)
            {
               for(int y = 0; y < grid.Height; y++)
                {
                    if(x == 0 || x == grid.Height - 1 || y == 0 || y == grid.Length - 1)
                    {
                        Assert.IsTrue(grid.Tiles[x, y] is Wall, "For coordinates ("+x+","+y+").");
                    }
                    else
                    {
                        Assert.IsTrue(grid.Tiles[x, y] == null || grid.Tiles[x, y] is ICollidable, "For coordinates("+x+", "+y+").");
                    }
                }
            }
        }
    }
}
