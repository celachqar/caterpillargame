﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NibblerBackend;
using NibblerBackend.Collidables;

namespace NibblerUnitTests
{
    /*
     * Caterpillar are created with MockGrids which don't have any token in them to be able to test without test having unpredicted result because Caterpillar collided in something!
     * */
    [TestClass]
    public class CaterpillarTest
    {
        [TestMethod]
        public void Constructor()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(); 
            Assert.IsInstanceOfType(c, typeof(Caterpillar));
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Points
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [TestMethod]
        public void Points_Length()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            Assert.AreEqual(c.Points.Count(), Caterpillar.DefaultLength);
        }

        /*Test position of Caterpillar depending on different grid dimensions*/
        [TestMethod]
        public void Points_GridWithDifferentDimensions()
        {
            //Arrange----------------------------------
            int gridWidth = 100;
            int gridHeight = 40;
            
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);
            Point caterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Points should be on same x as head, but lower than the head.
            Point p1 = caterpillarHead;
            Point p2 = new Point(caterpillarHead.X, caterpillarHead.Y + 1);
            Point p3 = new Point(caterpillarHead.X, caterpillarHead.Y + 2);
            Point p4 = new Point(caterpillarHead.X, caterpillarHead.Y + 3);
            Point p5 = new Point(caterpillarHead.X, caterpillarHead.Y + 4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test position of Caterpillar depending on different grid dimensions*/
        [TestMethod]
        public void Points_GridWithMinimalDimensions()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;

            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);
            Point caterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Points should be on same x as head, but lower than the head.
            Point p1 = caterpillarHead;
            Point p2 = new Point(caterpillarHead.X, caterpillarHead.Y + 1);
            Point p3 = new Point(caterpillarHead.X, caterpillarHead.Y + 2);
            Point p4 = new Point(caterpillarHead.X, caterpillarHead.Y + 3);
            Point p5 = new Point(caterpillarHead.X, caterpillarHead.Y + 4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test position of Caterpillar depending on different grid dimensions*/
        [TestMethod]
        public void Points_GridWithOddDimensions()
        {
            //Arrange----------------------------------
            int gridWidth = 99;
            int gridHeight = 21;
            
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);
            Point caterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Points should be on same x as head, but lower than the head.
            Point p1 = caterpillarHead;
            Point p2 = new Point(caterpillarHead.X, caterpillarHead.Y + 1);
            Point p3 = new Point(caterpillarHead.X, caterpillarHead.Y + 2);
            Point p4 = new Point(caterpillarHead.X, caterpillarHead.Y + 3);
            Point p5 = new Point(caterpillarHead.X, caterpillarHead.Y + 4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Update
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /*Test position of Caterpillar after moving of one spot that is free (has no Collidable) */
        [TestMethod]
        public void Update_InSpace() 
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth); //Made sure grid doesn't have any token

            //Initial position
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);
            //After one update, with defaultdirection UP, should move up
            caterpillarHeadExpectedY = caterpillarHeadExpectedY - 1;

            Point caterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Points should be on same x as head, but lower than the head.
            Point p1 = caterpillarHead;
            Point p2 = new Point(caterpillarHead.X, caterpillarHead.Y + 1);
            Point p3 = new Point(caterpillarHead.X, caterpillarHead.Y + 2);
            Point p4 = new Point(caterpillarHead.X, caterpillarHead.Y + 3);
            Point p5 = new Point(caterpillarHead.X, caterpillarHead.Y + 4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            c.Update();
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test if SelfCollision is triggered when a Caterpillar collides in itself.*/
        /*
         * Before :
         *  X------------------X
            X------------------X
            X--------CCC-------X
            X--------C-C-------X
            X--------C-C-------X
            X--------C---------X
            X------------------X
            X------------------X
            After: Direction changed to LEFT + 2 steps => SelfCollision!
         * */
        [TestMethod]
        public void Update_InSelf()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);  //Grid doesn't have any token so can freely Update

            //Arrange Caterpillar in U shape
            c.Grow(3);
            c.ChangeDirection(Direction.RIGHT);
            c.Update();
            c.Update();
            c.ChangeDirection(Direction.DOWN);
            c.Update();
            c.Update();
            Console.WriteLine(c);
            //Register if SelfCollision event is triggered
            int receivedEvents =0;
            c.SelfCollision += delegate (){receivedEvents++;};
            Console.WriteLine(c);
            //Act----------------------------------
            c.ChangeDirection(Direction.LEFT);
            c.Update();
            c.Update();

            //Assert----------------------------------
            Assert.AreEqual(1, receivedEvents);
        }

        /*Test if Collision is triggered when Caterpillar moving of one spot in a wall */
        [TestMethod]
        public void Update_InWall()
        {
            //Arrange----------------------------------
            Wall w = new Wall();
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillarWithCollidableAbove(w);

            //Register if Wall.Collision event is triggered
            List<string> receivedEvents = new List<string>();
            ICollidable actual = null;
            w.Collision += delegate (ICollidable element)
            {
                receivedEvents.Add(element.ToString());
                actual = element;
            };

            //Act----------------------------------
            c.Update(); //Caterpillar collides into wall

            //Assert----------------------------------
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, receivedEvents.Count);
            Assert.AreEqual(w, actual);
        }

        /*Test if Collision is triggered when Caterpillar moving of one spot in a CaterpillarGrower */
        [TestMethod]
        public void Update_InCaterpillarGrower_TestCollision()
        {
            //Arrange----------------------------------
            CaterpillarGrower grower = new CaterpillarGrower(0);
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillarWithCollidableAbove(grower);

            //Register if the ICollidable Collision event is triggered
            List<string> receivedEvents = new List<string>();
            ICollidable actual = null;
            grower.Collision += delegate (ICollidable element)
            {
                receivedEvents.Add(element.ToString());
                actual = element;
            };

            //Act----------------------------------
            c.Update(); //Caterpillar collides into grower

            //Assert----------------------------------
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, receivedEvents.Count);
            Assert.AreEqual(grower, actual);
        }

        /*Test if Collision is triggered when Caterpillar moves of one spot in a Caterpillar Shrinker */
        [TestMethod]
        public void Update_InCaterpillarShrinker_TestCollision()
        {
            //Arrange----------------------------------
            CaterpillarShrinker shrinker = new CaterpillarShrinker(0);
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillarWithCollidableAbove(shrinker);

            //Register if the ICollidable Collision event is triggered
            List<string> receivedEvents = new List<string>();
            ICollidable actual = null;
            shrinker.Collision += delegate (ICollidable element)
            {
                receivedEvents.Add(element.ToString());
                actual = element;
            };

            //Act----------------------------------
            c.Update(); //Caterpillar collides into shrinker

            //Assert----------------------------------
            Assert.IsNotNull(actual);
            Assert.AreEqual(1, receivedEvents.Count);
            Assert.AreEqual(shrinker, actual);
        }

        /*Test size of Caterpillar after colliding with a CaterpillarGrower */
        [TestMethod]
        public void Update_InCaterpillarGrower_TestGrowth()
        {
            //Arrange----------------------------------
            int growth = 2;
            CaterpillarGrower grower = new CaterpillarGrower(growth);
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillarWithCollidableAbove(grower);
            int expected = Caterpillar.DefaultLength + growth;

            //Act----------------------------------
            //Allow it to grow and take extra steps to make sure it doesn't keep growing
            for (int i=0; i < growth+2; i++)
            {
                c.Update();
            }
            int actual = c.Points.Count();

            //Assert----------------------------------
            Assert.AreEqual(expected, actual);
        }

        /*Test size of Caterpillar after colliding with a CaterpillarShrinker. 
         * Make sure that it never shrinks less than DefaultLength*/
        [TestMethod]
        public void Update_InCaterpillarShrinker_TestMaximumShrink()
        {
            //Arrange----------------------------------
            int shrink = 12;
            
            CaterpillarShrinker shrinker = new CaterpillarShrinker(shrink);
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillarWithCollidableAbove(shrinker);
            
            int expected = Caterpillar.DefaultLength - shrink;
            if (expected < Caterpillar.DefaultLength) expected = Caterpillar.DefaultLength;

            //Act----------------------------------
            //Allow it to grow and take extra steps to make sure it doesn't keep growing
            c.Update(); //Caterpillar collides into shrinker
            c.Update();
  
            int actual = c.Points.Count();

            //Assert----------------------------------
            Assert.AreEqual(expected, actual);
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Grow
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*Test exception throwing/input validation of Grow()*/
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void Grow_NegativeArgument()
        {
            //Arrange----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();

            //Act----------------------------------
            c.Grow(-1);
        }

        /*Test size of Caterpillar after Grow(n)*/
        [TestMethod]
        public void Grow_PositiveArgument()
        {
            //Arrange----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(600, 600);  //Made sure grid doesn't have any token
            int growNb = 200;
            int expectedPointsLength = Caterpillar.DefaultLength + growNb;

            //Act----------------------------------
            c.Grow(growNb);
            for (int i = 0; i < growNb + 50; i++)
            {
                c.Update();
            }
            int actualPointsLength = c.Points.Count();

            //Assert----------------------------------
            Assert.AreEqual(expectedPointsLength, actualPointsLength);
        }

        /*Test size of Caterpillar after Grow(0)*/
        [TestMethod]
        public void Grow_ZeroArgument()
        {
            //Arrange----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(600, 600);  //Made sure grid doesn't have any token
            int growNb = 0;
            int expectedPointsLength = Caterpillar.DefaultLength + growNb;

            //Act----------------------------------
            c.Grow(growNb);
            for (int i = 0; i < growNb + 50; i++)
            {
                c.Update();
            }
            int actualPointsLength = c.Points.Count();

            //Assert----------------------------------
            Assert.AreEqual(expectedPointsLength, actualPointsLength);
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|ChangeDirection
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /*Test when a Caterpillar is in a straight line and it changes Direction that is not the reverse Direction.*/
        /*Initial
         *  X------------------X
            X--------C---------X
            X--------C---------X
            X--------C---------X
            X--------C---------X
            X--------C---------X
            X------------------X
            Direction: UP

            After Act:
            X------------------X
            X--------CCCCC-----X
            X------------------X
            X------------------X
            X------------------X
            X------------------X
            X------------------X
            Direction: RIGHT
         */
        [TestMethod]
        public void ChangeDirection_StraightShape_NotSameNotOpposite()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);

            //Caterpillar changes direction and takes steps
            int nbOfSteps = 4;
            Direction newDirection = Direction.RIGHT;
            caterpillarHeadExpectedX = caterpillarHeadExpectedX + nbOfSteps;

            Point expectedCaterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            Point p1 = expectedCaterpillarHead;
            Point p2 = new Point(expectedCaterpillarHead.X -1, expectedCaterpillarHead.Y);
            Point p3 = new Point(expectedCaterpillarHead.X -2, expectedCaterpillarHead.Y);
            Point p4 = new Point(expectedCaterpillarHead.X -3, expectedCaterpillarHead.Y);
            Point p5 = new Point(expectedCaterpillarHead.X -4, expectedCaterpillarHead.Y);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };
            
            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();  //Grid doesn't have any token so can freely Update
            c.ChangeDirection(newDirection); //Change direction from initial (UP) to RIGHT

            for (int i = 0; i < nbOfSteps; i++)
            {
                c.Update();
            }

            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test when a Caterpillar is in a straight line and it changes Direction that is the same Direction.*/
        [TestMethod]
        public void ChangeDirection_StraightShape_SameDirection()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;

            //Caterpillar should take steps in same direction
            int nbOfSteps = 4;
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) - (Caterpillar.DefaultLength - 1);

            caterpillarHeadExpectedY = caterpillarHeadExpectedY - nbOfSteps;  //After updates
            Point expectedCaterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Other points should be in same x, beneath
            Point p1 = expectedCaterpillarHead;
            Point p2 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y+1);
            Point p3 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y+2);
            Point p4 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y+3);
            Point p5 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y+4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.ChangeDirection(Caterpillar.DefaultDirection); //Same direction
            for (int i = 0; i < nbOfSteps; i++) c.Update();
            
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test when a Caterpillar is in a straight line and it changes to reverse Direction.*/
        [TestMethod]
        public void ChangeDirection_StraightShape_Reverse()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;

            //Caterpillar will reverse direction and take steps
            int nbOfSteps = 6;
            Direction newDirection = Direction.DOWN;
            int caterpillarHeadExpectedX = gridWidth / 2 - 1;
            int caterpillarHeadExpectedY = (gridHeight / 2 + 1) + nbOfSteps;
            Point expectedCaterpillarHead = new Point(caterpillarHeadExpectedX, caterpillarHeadExpectedY);

            //Other points shoule be on same x, but higher than head.
            Point p1 = expectedCaterpillarHead;
            Point p2 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y - 1);
            Point p3 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y - 2);
            Point p4 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y - 3);
            Point p5 = new Point(expectedCaterpillarHead.X, expectedCaterpillarHead.Y - 4);
            List<Point> expected = new List<Point> { p1, p2, p3, p4, p5 };

            //Act----------------------------------
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.ChangeDirection(newDirection); //Change direction from default (UP) to DOWN

            //Advance many steps
            for (int i = 0; i < nbOfSteps; i++) c.Update();
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test when a Caterpillar is in a "U" shape and it changes to same Direction ant takes many steps.*/
        /* H: Head 
         * Before test
         *  X------------------X
            X------------------X
            X--------CCC-------X
            X--------C-C-------X
            X--------C-H-------X
            X--------C---------X
            X------------------X
            X------------------X
            X------------------X
            Direction : DOWN
            Head : (11,9)
            Points Count : 8
            Points: {(9,10) (9,9) (9,8) (9,7) (10,7) (11,7) (11,8) (11,9)}

            After Test
            X------------------X
            X------------------X
            X--------CCC-------X
            X----------C-------X
            X----------C-------X
            X----------C-------X
            X----------C-------X
            X----------H-------X
            X------------------X
            Direction : DOWN (same)
            Head : (11,9)
            Points Count : 8
            Points: {(9,7) (10,7) (11,7) (11,8) (11,9) (11,10) (11, 11) (11,12)}
         * */
        [TestMethod]
        public void ChangeDirection_UShape_SameDirection()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);  //Grid doesn't have any token so can freely Update

            //Arrange Caterpillar pre-test
            int nbOfSteps = 3;

            c.Grow(3);
            c.ChangeDirection(Direction.RIGHT);
            c.Update();
            c.Update();
            c.ChangeDirection(Direction.DOWN);
            c.Update();
            c.Update();
  
            //After test, Caterpillar should have length 5+3, have advanced of 3 points in same direction
            List<Point> expected = new List<Point> { new Point(9, 7), new Point(10, 7), new Point(11, 7), new Point(11, 8), new Point(11, 9), new Point(11, 10), new Point(11, 11), new Point(11, 12) };

            //Act----------------------------------
            c.ChangeDirection(Direction.DOWN);
            for (int i = 0; i < nbOfSteps; i++) c.Update();
            List<Point> actual = c.Points.ToList();

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }


        /*Test when a Caterpillar is in a "U" shape and it changes to a not-same-not-opposite Direction and takes many steps.*/
        /* H: Head 
         * Before test
         *  X------------------X
            X------------------X
            X--------CCC-------X
            X--------C-C-------X
            X--------C-H-------X
            X--------C---------X
            X------------------X
            X------------------X
            X------------------X
            Direction : DOWN
            Head : (11,9)
            Points Count : 8
            Points: {(9,10) (9,9) (9,8) (9,7) (10,7) (11,7) (11,8) (11,9)}

            After Test
            X------------------X
            X------------------X
            X---------CC-------X
            X----------C-------X
            X----------CCCCH---X
            X------------------X
            X------------------X
            X------------------X
            X------------------X
            Direction : RIGHT
            Head : (15,9)
            Points Count : 8
            Points: {(10,7) (11,7) (11,8) (11,9) (12, 9) (13, 9) (14, 9) (15,9)}
         * */
        [TestMethod]
        public void ChangeDirection_UShape_NotSameNotOppositeDirection()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);  //Grid doesn't have any token so can freely Update

            //Arrange Caterpillar pre-test
            int nbOfSteps = 4;

            c.Grow(3);
            c.ChangeDirection(Direction.RIGHT);
            c.Update();
            c.Update();
            c.ChangeDirection(Direction.DOWN);
            c.Update();
            c.Update();
            Console.WriteLine(c);

            //After test, Caterpillar should have length 5+3, have advanced of 3 points in same direction
            List<Point> expected = new List<Point> {new Point(10, 7), new Point(11, 7), new Point(11, 8), new Point(11, 9), new Point(12, 9), new Point(13, 9), new Point(14, 9), new Point(15,9)};

            //Act----------------------------------
            c.ChangeDirection(Direction.RIGHT);
            for (int i = 0; i < nbOfSteps; i++) c.Update();
            List<Point> actual = c.Points.ToList();
            Console.WriteLine(c);

            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }


        /*Test when a Caterpillar is in a "U" shape and it changes to the reverse Direction ant take one step.*/
        /* H: Head 
         * Before
         *  X------------------X
            X------------------X
            X--------CCC-------X
            X--------C-C-------X
            X--------C-H-------X
            X--------C---------X
            X------------------X
            X------------------X
            Direction:DOWN

            After
            X------------------X
            X------------------X
            X--------CCC-------X
            X--------C-C-------X
            X--------C---------X
            X--------C---------X
            X--------H---------X
            X------------------X
            Direction:UP
         * */
        [TestMethod]
        public void ChangeDirection_UShape_OppositeDirectionFollowedWithOneStep()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);  //Grid doesn't have any token so can freely Update

            //Arrange Caterpillar in U shape
            c.Grow(3);
            c.ChangeDirection(Direction.RIGHT);
            c.Update();
            c.Update();
            c.ChangeDirection(Direction.DOWN);
            c.Update();
            c.Update();
            
            //Caterpillar should reverse direction and advance of one point
            List<Point> expected = new List<Point> { new Point(9, 11), new Point(9, 10), new Point(9, 9), new Point(9, 8), new Point(9, 7), new Point(10, 7), new Point(11, 7), new Point(11, 8) };

            //Act----------------------------------
            c.ChangeDirection(Direction.UP);
            c.Update();
            List<Point> actual = c.Points.ToList();
            
            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }

        /*Test when a Caterpillar is in a "U" shape and it changes to the reverse Direction.*/
        /* H : Head
         * Before
         *  X------------------X
            X------------------X
            X--------CC--------X
            X--------CC--------X
            X--------CH--------X
            X--------C---------X
            X------------------X
            X------------------X
            Direction : DOWN            
            Head : (10,9)
            Points Count : 7
            Points: {(9,10) (9,9) (9,8) (9,7) (10,7) (10,8) (10,9) }

            After
            X------------------X
            X------------------X
            X--------CC--------X
            X--------CC--------X
            X--------C---------X
            X--------C---------X
            X--------H---------X
            X------------------X
            Direction : UP (But DOWN really)
            Head : (9, 12)
            Points Count : 7
            Points: {(10,7) (9,7) (9,8) (9,9) (9,10) (9,11) (9,12)}

         * */
        [TestMethod]
        public void ChangeDirection_UShape_OppositeFollowedWithManySteps()
        {
            //Arrange----------------------------------
            int gridWidth = GameState.MinimumGridSize;
            int gridHeight = GameState.MinimumGridSize;
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar(gridHeight, gridWidth);  //Grid doesn't have any token so can freely Update
            int nbOfSteps = 3;
            //Arrange Caterpillar in U shape
            c.Grow(2);
            c.ChangeDirection(Direction.RIGHT);
            c.Update();
            c.ChangeDirection(Direction.DOWN);
            c.Update();
            c.Update();
            Console.WriteLine(c);

            //Caterpillar should reverse direction and advance of 2 points
            List<Point> expected = new List<Point> { new Point(9, 13), new Point (9,12), new Point(9, 11), new Point(9, 10), new Point(9, 9), new Point(9, 8), new Point(9, 7)};

            //Act----------------------------------
            c.ChangeDirection(Direction.UP);
            for(int i=0; i<nbOfSteps; i++)
            {
                c.Update();
            }
 
            List<Point> actual = c.Points.ToList();
            Console.WriteLine(c);
            //Assert----------------------------------
            CollectionAssert.AreEquivalent(expected, actual);
        }


        //TODO


        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Die
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [TestMethod]
        public void Die_WhenAlive()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.Die();
            Assert.AreEqual(0, c.Points.Count());
        }

        [TestMethod]
        public void Die_WhenAlreadyDead()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.Die();
            c.Die();
            Assert.AreEqual(0, c.Points.Count());
        }

        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //|Reset
        //|/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void Reset_WhenAlive()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.Grow(10);
            c.Reset();
            Assert.AreEqual(Caterpillar.DefaultLength, c.Points.Count());
        }

        [TestMethod]
        public void Reset_WhenDead()
        {
            Caterpillar c = CaterpillarTestUtilities.CreateCaterpillar();
            c.Die();
            c.Reset();
            Assert.AreEqual(Caterpillar.DefaultLength, c.Points.Count());
        }
        
    }
}
