﻿using System.Collections.Generic;
using System.Linq;
using NibblerBackend;

namespace NibblerUnitTests
{
    /*Class used for testing. Mocking a Grid that doesn't see tokens unless explicitely specified (SetMockGridWithTokenAboveCaterpillar())*/
    class MockGrid : Grid
    {
        public MockGrid(int hei, int len, double stgc, double[] btg, double[] stg): base(hei, len, stgc, btg, stg)
        {}

        /*Make sure no token is added in any method*/
        public override void SetGrid(IEnumerable<Point> caterpillarPoints)
        {
            this.caterpillarPoints = caterpillarPoints;
        }

        /*Version of SetGrid where there is no token added*/
        public void SetMockGrid(IEnumerable<Point> caterpillarPoints)
        {
            this.caterpillarPoints = caterpillarPoints;
        }

        /*Version of SetGrid where there is a Collidable just above the caterpillar*/
        public void SetMockGridWithTokenAboveCaterpillar(IEnumerable<Point> caterpillarPoints, ICollidable collidable)
        {
            this.caterpillarPoints = caterpillarPoints;

            Point frontElement = this.caterpillarPoints.ElementAt(this.caterpillarPoints.Count() - 1);

            Point collidablePoint = new Point(frontElement.X, frontElement.Y - 1);
            Tiles[collidablePoint.X, collidablePoint.Y] = collidable;
            this.CurrentTokenPosition = collidablePoint;
        }
    }
}
