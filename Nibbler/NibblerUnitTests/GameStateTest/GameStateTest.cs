﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NibblerBackend;
using NibblerBackend.Collidables;

namespace NibblerUnitTests
{
    [TestClass]
    public class GameStateTest
    {
        public void LoadTest()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");

            Assert.IsInstanceOfType(gamestate, typeof(GameState));
        }
        
        /*Testing the TokenAdded event is fired when a new token is added*/
        [TestMethod]
        public void Events_TokenAdded()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");

            //Calls the token added event, gamestate then sets up events for this new token in _onTokenAdded
            gamestate.GameGrid.AddNewToken(null);

            //Check that the token position is properly set (therefore it is not null)
            Assert.IsNotNull(gamestate.GameGrid.Tiles[gamestate.GameGrid.CurrentTokenPosition.X, gamestate.GameGrid.CurrentTokenPosition.Y]);
            
            gamestate.GameGrid.Tiles[gamestate.GameGrid.CurrentTokenPosition.X, gamestate.GameGrid.CurrentTokenPosition.Y].Collide(gamestate.GameCaterpillar);
            gamestate.Update();
            //Check that the length of the caterpillar changed, that means the token was properly initialized
            Assert.AreNotEqual(5, gamestate.GameCaterpillar.Points.Count());
        }
        
        /*Testing the NoMoreLives event is fired when a Caterpillar doesn't have any more lives left*/
        [TestMethod]
        public void Events_NoMoreLives()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");
            Caterpillar cp = gamestate.GameCaterpillar;
            while (gamestate.ScoreLives.Lives > 0) gamestate.ScoreLives.UpdatePointsAndLives(new Wall());

            //When caterpillar reaches 0 lives, no more lives event should be fired, meaning setPause will be called and shouldContinue should be false.
            Assert.IsFalse(gamestate.ShouldContinue);
        }
        
        /*Testing the Collison event is fired when Caterpillar collides in a wall*/
        [TestMethod]
        public void Events_Collision_Wall()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");
            Point tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
            ICollidable token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];

            //Make sure Caterpillar is not going to collide on anything else before wall
            while(tokenPosition.X == gamestate.GameCaterpillar.DefaultFrontElementX)
            {
                gamestate.GameGrid.AddNewToken(token);
                tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
                token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];
            }

            int nbOfTimesCollisionWIthWallHaveBeenRegistered = 0;
            ICollidable actualToken = null;
            ICollidable wallInFrontOfCaterpillar = gamestate.GameGrid[gamestate.GameCaterpillar.DefaultFrontElementX, 0];
            wallInFrontOfCaterpillar.Collision += delegate (ICollidable element)
            {
                actualToken = element;
                nbOfTimesCollisionWIthWallHaveBeenRegistered++;
            };

            //Act----------------------------------
            //Caterpillar should collide 3 times with wall
            while (gamestate.ShouldContinue)
            {
                gamestate.GameCaterpillar.Update();
                while (tokenPosition.X == gamestate.GameCaterpillar.DefaultFrontElementX)
                {
                    gamestate.GameGrid.AddNewToken(token);
                    tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
                    token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];
                }
            }

            //Assert----------------------------------
            Assert.AreEqual(3, nbOfTimesCollisionWIthWallHaveBeenRegistered);
            Assert.IsNotNull(actualToken);
            Assert.AreEqual(wallInFrontOfCaterpillar, actualToken);
        }

        /*Testing the Collison event is fired when Caterpillar collides in a CaterpillarShrinker*/
        [TestMethod]
        public void Events_Collision_CaterpillarShrinker()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");
            Point tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
            ICollidable token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];

            //Make sure we have a Caterpillar shrinker (and not grower)
            while(!(token is CaterpillarShrinker))
            {
                gamestate.GameGrid.AddNewToken(null);
                tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
                token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];
            }

            ICollidable actualToken = null;
            token.Collision += delegate (ICollidable element)
            {
                actualToken = element;
            };

            //Act----------------------------------
            token.Collide(gamestate.GameCaterpillar);

            //Assert----------------------------------
            Assert.IsNotNull(actualToken);
            Assert.AreEqual(token, actualToken);
        }

        /*Testing the Collison event is fired when Caterpillar collides in CaterpillarGrower wall*/
        [TestMethod]
        public void Events_Collision_CaterpillarGrower()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");
            Point tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
            ICollidable token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];

            //Make sure we have a Caterpillar shrinker (and not grower)
            while (!(token is CaterpillarShrinker))
            {
                gamestate.GameGrid.AddNewToken(null);
                tokenPosition = gamestate.GameGrid.CurrentTokenPosition;
                token = gamestate.GameGrid[tokenPosition.X, tokenPosition.Y];
            }

            ICollidable actualToken = null;
            token.Collision += delegate (ICollidable element)
            {
                actualToken = element;
            };

            //Act----------------------------------
            token.Collide(gamestate.GameCaterpillar);

            //Assert----------------------------------
            Assert.IsNotNull(actualToken);
            Assert.AreEqual(token, actualToken);
        }

        //NOTE: SelfCollision event tested in CaterpillarTest

        [TestMethod]
        public void SetPauseTest1()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerBackend/sampleGrid.txt");
            gamestate.ScoreLives.UpdatePointsAndLives(new Wall()); // Remove one life
            gamestate.ScoreLives.UpdatePointsAndLives(new Wall()); // Remove a second life
            gamestate.ScoreLives.UpdatePointsAndLives(new Wall()); // Remove the last life --> Score = 0, Lives = 0

            Assert.AreEqual(0, gamestate.GameCaterpillar.Points.Count()); // Will ensure all points of the caterpillar were removed
        }

        // Due to how our grids are validated, we only need to test this one extra file.
        // Our validation only checks if there are the same amount of characters in a rectangle
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void BadGridTest()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerUnitTests/GameStateTest/BadGrids/badSampleGrid.txt");
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void BadProbabilityTest1()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerUnitTests/GameStateTest/BadGrids/badProbability1.txt");
        }

        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void BadProbabilityTest2()
        {
            GameState gamestate = GameState.Load("/Nibbler/NibblerUnitTests/GameStateTest/BadGrids/badProbability2.txt");
        }
    }
}
