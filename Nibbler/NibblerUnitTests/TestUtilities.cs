﻿using NibblerBackend;

namespace NibblerUnitTests
{
    /*
     * Caterpillar are created with MockGrids which don't have any token in them to be able to test without test having unpredicted result because Caterpillar collided in something!
     * */
    public static class CaterpillarTestUtilities
    {

        private static double CaterpillarShrinkerGenProbability = 0.5;
        private static double[] CaterpillarGrowerGrowthProbabilities = new double[3] { 0.25, 0.25, 0.5 };
        private static double[] CaterpillarShrinkerGrowthProbabilities = new double[2] { 0.5, 0.5 };

        /*Caterpillar created on a grid with GameState.MinimumGridSize dimensions where no token is present*/
        public static Caterpillar CreateCaterpillar()
        {
            MockGrid grid = new MockGrid(GameState.MinimumGridSize, GameState.MinimumGridSize, CaterpillarShrinkerGenProbability, CaterpillarGrowerGrowthProbabilities, CaterpillarShrinkerGrowthProbabilities);
            Caterpillar c = new Caterpillar(grid);
            grid.SetMockGrid(c.Points);
            return c;
        }

        /*Caterpillar created on a grid with custom dimensions where no token is present*/
        public static Caterpillar CreateCaterpillar(int height, int width)
        {
            MockGrid grid = new MockGrid(height, width, CaterpillarShrinkerGenProbability, CaterpillarGrowerGrowthProbabilities, CaterpillarShrinkerGrowthProbabilities);
            Caterpillar c = new Caterpillar(grid);
            grid.SetMockGrid(c.Points);
            return c;
        }

        /*Caterpillar created on a grid where a token is placed just above Caterpillar*/
        public static Caterpillar CreateCaterpillarWithCollidableAbove(ICollidable collidable)
        {
            MockGrid grid = new MockGrid(GameState.MinimumGridSize, GameState.MinimumGridSize, CaterpillarShrinkerGenProbability, CaterpillarGrowerGrowthProbabilities, CaterpillarShrinkerGrowthProbabilities);
            Caterpillar c = new Caterpillar(grid);
            grid.SetMockGridWithTokenAboveCaterpillar(c.Points, collidable);
            return c;
        }
    }
}
