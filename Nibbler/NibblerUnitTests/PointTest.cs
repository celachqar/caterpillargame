﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NibblerBackend;

namespace NibblerUnitTests
{
    [TestClass]
    public class PointTest
    {
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void Constructor_Exception1()
        {
            Point p = new Point(1, -1);
        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void Constructor_Exception2()
        {
            Point p = new Point(-1, 1);
        }

        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        [TestMethod]
        public void Constructor_Exception3()
        {
            Point p = new Point(-1, -1);
        }

        [TestMethod]
        public void EqualsTest1()
        {
            Point p1 = new Point(1, 1);
            Point p2 = new Point(1, 1);

            Assert.IsTrue(p1.Equals(p2));
        }

        [TestMethod]
        public void EqualsTest2()
        {
            Point p1 = new Point(1, 2);
            Point p2 = new Point(1, 3);

            Assert.IsFalse(p1.Equals(p2));
        }

        [TestMethod]
        public void EqualsTest3()
        {
            Point p1 = new Point(2, 1);
            Point p2 = new Point(3, 1);

            Assert.IsFalse(p1.Equals(p2));
        }

        [TestMethod]
        public void EqualsTest4()
        {
            Point p1 = new Point(1, 2);
            Point p2 = new Point(3, 4);

            Assert.IsFalse(p1.Equals(p2));
        }

        [TestMethod]
        public void ToStringTest()
        {
            Point p = new Point(3, 4);

            Assert.AreEqual("(3,4)", p.ToString());
        }
    }
}
