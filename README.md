Assignment made for class 420-411-DW Software Development IV (Advanced .NET Programming with C#), taught by Swetha Rajagopal at Dawson College, in Winter 2019. <br/>
We were teams of 3 and had to follow the given UML.  <br/>
I do not recall exactly what are my contributions but I know the NibblerBackend/Caterpillar.cs class is my work! <br/>

### Assignment goals   <br/>
* Learn to code with a Queue data structure.
* Learn delegates, events and event-driven programming.
* Learn a gaming framework (game loop, sprites, etc).


### Coded in
* C# / .NET Framework 4.5 (so it works with Monogame)
* With the Monogame Framework [3.7.1](http://community.monogame.net/t/monogame-3-7-1-release/11173)


# Game

Based on the old-scool arcade [Nibbler game](https://classicreload.com/qbasic-nibbles.html)

*You are a poop caterpillar who lives in a toilet bowl... <br/>
Use the keyboard arrows to try to live as long as possible without hitting the toilet walls or yourself! <br/>
Toilet paper makes you shrink and corn makes you grow. <br/>
Good luck! <br/>*


![Game Screenshot](Caterpillar1.JPG)

Graphics by @LeeYumHarbec